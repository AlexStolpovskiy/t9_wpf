﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace T9_SpellingWpf
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        string[] T9 = {"0", "0", "abc", "def", "ghi",
             "jkl", "mno", "pqrs", "tuv", "wxyz"};

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "Text files (*.txt)|*.txt";
            openFile.ShowDialog();
            string[] input = File.ReadAllLines(openFile.FileName);

            int countCases = int.Parse(countCasesTB.Text);
            if (countCases <= input.Length)
            {
                StreamWriter sw = new StreamWriter(new FileStream("output.txt", FileMode.OpenOrCreate));
                for (int c = 0; c < countCases; c++)
                {
                    sw.Write("{0}\t\t Case #{1}: {2}", input[c], c + 1, ConvertT9(input[c]) + "\n");
                }

                sw.Flush();
                sw.Close();
                outputTB.Text = File.ReadAllText("output.txt");
            }
            else
                MessageBox.Show("Exceeded the number of cases entered!!!");
        }

        public string ConvertT9(string input)
        {
            int i = 0, j, k, l = 0;
            string result = "";

            while (i < input.Length)
            {
                if (input[i] == ' ')
                {
                    ++i;
                    continue;
                }
                //j = counter variable to iterate T9 string arr
                for (j = 0; j < 10; ++j)
                {
                    //find the position of the char
                    //within the mapped pad[T9]
                    l = T9[j].IndexOf(input.Substring(i, 1));

                    //break point
                    if (l != -1)
                        break;
                }

                //print out the number to press
                //l = how many times to press to get a j key
                for (k = 0; k <= l; ++k)
                    result += j.ToString();
                i++;
                result += " ";
            }

            return result;
        }
    }
}
